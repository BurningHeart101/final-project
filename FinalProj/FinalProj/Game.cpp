#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"
#include "Game.h"

#include <assert.h>
#include <sstream>
#include <iomanip>			//for altering displayed text

using namespace std;
using namespace sf;

/*
initialises everything needed for the game
window: window that the game is displayed in
pre-con: game booted up
post-con: everything has been initialised with the correct properties
*/
void Game::Init(RenderWindow& window)
{
	//Load font
	if (!font.loadFromFile("data/fonts/comic.ttf"))
		assert(false);

	//Initialise the main player
	LoadTexture("data/Character.png", charTex);
	Play character;
	character.Init(window, charTex, Play::ObjT::PLAYER, Play::EnemyT::OTHER);
	objects.push_back(character);

	//Initialise bullets
	LoadTexture("data/Bullet.png", bulletTex);
	for (int i = 0; i < 4; ++i)
	{
		Play bullet;
		bullet.Init(window, bulletTex, Play::ObjT::BULLET, Play::EnemyT::OTHER);
		objects.push_back(bullet);
	}

	//Initialise enemy
	LoadTexture("data/Square_Enemy.png", eSquareTex);
	for (int i = 0; i < 2; ++i)
	{
		Play eSquare;
		eSquare.Init(window, eSquareTex, Play::ObjT::ENEMY, Play::EnemyT::SQUARE);
		objects.push_back(eSquare);
	}
	//Create sprite for use on start screen
	startSquare.setTexture(eSquareTex);
	startSquare.setOrigin(eSquareTex.getSize().x / 2.f, eSquareTex.getSize().y / 2.f);
	startSquare.setScale(1.5, 1.5);

	LoadTexture("data/Triangle_Enemy.png", eTriangleTex);
	for (int i = 0; i < 6; ++i)
	{
		Play eTriangle;
		eTriangle.Init(window, eTriangleTex, Play::ObjT::ENEMY, Play::EnemyT::TRIANGLE);
		objects.push_back(eTriangle);
	}

	//Create sprite for use on start screen
	startTriangle.setTexture(eTriangleTex);
	startTriangle.setOrigin(eTriangleTex.getSize().x / 2.f, eTriangleTex.getSize().y / 2.f);
	startTriangle.setScale(1.5, 1.5);

	LoadTexture("data/Circle_Enemy.png", eCircleTex);
	for (int i = 0; i < 3; ++i)
	{
		Play eCircle;
		eCircle.Init(window, eCircleTex, Play::ObjT::ENEMY, Play::EnemyT::CIRCLE);
		objects.push_back(eCircle);
	}
	//Create sprite for use on start screen
	startCircle.setTexture(eCircleTex);
	startCircle.setOrigin(eCircleTex.getSize().x / 2.f, eCircleTex.getSize().y / 2.f);
	startCircle.setScale(1.5, 1.5);

	//Initialise background
	LoadTexture("data/Background.png", bgndTex);
	bgndTex.setRepeated(true);
	bgndSpr.setTexture(bgndTex);
	bgndSpr.setOrigin(bgndTex.getSize().x / 2.f, bgndTex.getSize().y / 2.f);
	bgndSpr.setPosition(window.getSize().x / 2.f, window.getSize().y / 2.f);

	//Initilaise menu music
	if (!menuMusic.openFromFile("data/menumusic.wav"))
		assert(false);
	//Start game with menu music
	menuMusic.setVolume(50);
	menuMusic.setLoop(true);
	menuMusic.play();

	//Initilaise background music
	if (!bgMusic.openFromFile("data/bgmusic.wav"))
		assert(false);
	//Play background music during gameplay
	bgMusic.setVolume(50);
	bgMusic.setLoop(true);

	//Initalise player sound
	if (!playerBuffer.loadFromFile("data/playerHurt.wav"))
		assert(false);
	playerHurt.setBuffer(playerBuffer);


	//Initalise bullet sound
	if (!bulletBuffer.loadFromFile("data/shoot.wav"))
		assert(false);
	bulletShoot.setBuffer(bulletBuffer);

	//Initalise enemy sound
	if (!enemyBuffer.loadFromFile("data/enemyHurt.wav"))
		assert(false);
	enemyHurt.setBuffer(enemyBuffer);

	//Load highscore database
	metrics.Load("data/score.db", true);
}

/*
updates screen the player is currently seeing
window: window that the game is displayed in
key: last key pressed on keyboard
pre-con: one game cycle has passed
post-con: correct screen has been updated
*/
void Game::Update(RenderWindow& window, char key)
{
	//Get time passed since last call
	elapsed = clock.getElapsedTime().asSeconds(); 
	clock.restart();


	switch (screen)
	{
		//Default
		case Mode::START:
			UpdateStart(key);
			break;

		case Mode::PLAY:
			UpdatePlay(window, key);
			break;

		case Mode::PAUSE:
			if (key == GC::SPACE_BAR)
				screen = Mode::PLAY;
			break;

		case Mode::ENTER_NAME:
			transition += elapsed;
			UpdateEnterName(key);
			break;

		case Mode::END:
			if (key == GC::SPACE_BAR)
				screen = Mode::START;
			break;

		default:
			assert(false);
	}
}

/*
updates start screen
key: last key pressed on keyboard
pre-con: current screen is START
post-con: start screen properly updated
*/
void Game::UpdateStart(char key)
{
	if (key == GC::SPACE_BAR)
	{
		//Reset character
		objects[0].active = true;
		objects[0].spr.setRotation(GC::I_ZERO);

		//Reset timer for name entry
		transition = GC::F_ZERO;

		//Reset stats
		metrics.Restart();

		//Change music
		menuMusic.stop();
		bgMusic.play();
		screen = Mode::PLAY;
	}
}

/*
updates game objects & properties used in gameplay
window: window that the game is displayed in
key: last key pressed on keyboard
pre-con: current screen is PLAY
post-con: all objects & properties in gameplay have been updated
*/
void Game::UpdatePlay(RenderWindow& window, char key)
{
	//Update spawn timers
	enemySpawnTimer += elapsed;
	bulletSpawnTimer += elapsed;

	//Check for object collisions
	CheckCollisions(window);

	for (size_t i = 0; i < objects.size(); ++i)
		objects[i].Update(window, elapsed, objects, bulletSpawnTimer, enemySpawnTimer, bulletShoot);

	//End game
	if (metrics.lives <= GC::I_ZERO)
	{
		//Disable all objects
		for (size_t i = 0; i < objects.size(); ++i)
			objects[i].active = false;

		//Update music
		bgMusic.stop();
		menuMusic.play();

		screen = Mode::ENTER_NAME;
	}

	//Pause game
	if (key == GC::SPACE_BAR)
		screen = Mode::PAUSE;
}

/*
updates ENTER_NAME screen 
key: last key pressed on keyboard
pre-con: current screen is ENTER_NAME
post-con: ENTER_NAME screen updated properly
*/
void Game::UpdateEnterName(char key)
{
	//Change music

	if (transition >= 1.f) //Stop accidental inputs from gameplay
	{
		//Input character
		if (key != GC::INVALID_KEY && key != GC::SPACE_BAR)
			metrics.name += key;

		//Organise and display leaderboard
		if (metrics.name.size() == GC::NAME_SIZE)
		{
			metrics.SortAndUpdatePlayerData();
			metrics.Save();

			screen = Mode::END;
		}
	}
}

/*
check to see if any objects are colliding with each other
window: window that the game is displayed in
objects: vector that stores all game objects inside
debug: state of whether debug is enabled or disabled
pre-con: one game cycle has passed
post-con: collision checks have been done, any colliding objects are properly updated
*/
void Game::CheckCollisions(RenderWindow& window)
{
	if (objects.size() > GC::MIN_SIZE)
	{
		for (size_t i = 0; i < objects.size(); ++i)
		{
			Play& a = objects[i]; //First object
			if (a.active)
			{
				if (i < objects.size() - 1) //Final object in vector will have already done every check
					for (size_t j = i + 1; j < objects.size(); ++j)
					{
						Play& b = objects[j]; // Second object
						if (b.active)
						{
							//Check if object A and object B are colliding 
							if (CircleToCircle(a.spr.getPosition(), b.spr.getPosition(), a.radius + b.radius))
							{
								a.colliding = true;
								b.colliding = true;
								a.Hit(b, metrics.lives, metrics.score, playerHurt, enemyHurt);
								b.Hit(a, metrics.lives, metrics.score, playerHurt, enemyHurt);
							}
						}
					}
			}
		}
	}
}

/*
renders screen the player is currently seeing
window: window that the game is displayed in
pre-con: one game cycle has passed
post-con: correct screen has been rendered
*/
void Game::Render(RenderWindow& window)
{
	switch(screen)
	{
		case Mode::START:
			RenderStartHUD(window);
			break;

		case Mode::PLAY:
			//Render background
			window.draw(bgndSpr);

			//Render game objects
			for (size_t i = 0; i < objects.size(); ++i)
				objects[i].Render(window, debug);

			RenderPlayHUD(window);
			break;

		case Mode::PAUSE:
			//Render background
			window.draw(bgndSpr);

			//Render game objects
			for (size_t i = 0; i < objects.size(); ++i)
				objects[i].Render(window, false);

			RenderPauseHUD(window);
			break;

		case Mode::ENTER_NAME:
			RenderEnterNameHUD(window);
			break;

		case Mode::END:
			RenderEndHUD(window);
			break;

		default:
			assert(false);
	}
}

/*
renders start screen text
window: window that the game is displayed in
pre-con: one game cycle has passed, mode = START
post-con: text has been rendered to the start screen
*/
void Game::RenderStartHUD(RenderWindow& window)
{
	stringstream ss;
	float x = window.getSize().x / 2.f;
	float y;

	//Draw title
	Text titleTxt("Shapeshooter", font, 100);
	FloatRect Tfr = titleTxt.getGlobalBounds();
	titleTxt.setPosition(x - Tfr.width / 2.f, 0);
	window.draw(titleTxt);

	//Draw instructions
	Text txt("A and D to rotate", font, 50);
	FloatRect fr = txt.getGlobalBounds();
	txt.setPosition(x - fr.width / 2.f, Tfr.height + 50);
	window.draw(txt);

	txt.setString("K to shoot");
	fr = txt.getGlobalBounds();
	y = txt.getPosition().y + fr.height + 20;
	txt.setPosition(x - fr.width / 2.f, y);
	window.draw(txt);

	txt.setString("SPACE to pause");
	fr = txt.getGlobalBounds();
	y = txt.getPosition().y + fr.height + 20;
	txt.setPosition(x - fr.width / 2.f, y);
	window.draw(txt);

	y += fr.height + 80;
	float tempY = y;

	//Draw triangle info
	x = window.getSize().x * 0.25f;
	startTriangle.setPosition(x, y);
	window.draw(startTriangle);

	y += eTriangleTex.getSize().y / 2.f + 20;
	ss << GC::TRIANGLE_SCORE << " points";
	txt.setString(ss.str());
	fr = txt.getGlobalBounds();
	txt.setPosition(x - fr.width / 2.f, y);
	window.draw(txt);
	ss.str("");

	y += fr.height + 20;
	txt.setString("Swarm");
	fr = txt.getGlobalBounds();
	txt.setPosition(x - fr.width / 2.f, y);
	window.draw(txt);

	//Draw square info
	y = tempY;
	x = window.getSize().x * 0.5f;
	startSquare.setPosition(x, y);
	window.draw(startSquare);

	y += eSquareTex.getSize().y / 2.f + 20;
	ss << GC::SQUARE_SCORE << " points";
	txt.setString(ss.str());
	fr = txt.getGlobalBounds();
	txt.setPosition(x - fr.width / 2.f, y);
	window.draw(txt);
	ss.str("");

	y += fr.height + 20;
	txt.setString("Tank");
	fr = txt.getGlobalBounds();
	txt.setPosition(x - fr.width / 2.f, y);
	window.draw(txt);

	//Draw circle info
	y = tempY;
	x = window.getSize().x * 0.75f;
	startCircle.setPosition(x, y);
	window.draw(startCircle);

	y += eCircleTex.getSize().y / 2.f + 20;
	ss << GC::CIRCLE_SCORE << " points";
	txt.setString(ss.str());
	fr = txt.getGlobalBounds();
	txt.setPosition(x - fr.width / 2.f, y);
	window.draw(txt);
	ss.str("");

	y += fr.height + 20;
	txt.setString("Miniature");
	fr = txt.getGlobalBounds();
	txt.setPosition(x - fr.width / 2.f, y);
	window.draw(txt);

	//Draw start instruction
	txt.setString("Press SPACE to start");
	fr = txt.getGlobalBounds();
	y = window.getSize().y  * 0.95 - fr.height;
	txt.setPosition(window.getSize().x / 2.f - fr.width / 2.f, y);
	window.draw(txt);
}

/*
render gameplay screen text
window: window that the game is displayed in
pre-con: one game cycle has passed, mode = PLAY
post-con: text has been rendered to the gameplay screen
*/
void Game::RenderPlayHUD(RenderWindow& window)
{
	float x;
	stringstream ss;

	//Draw player health
	ss << "Lives: " << metrics.lives;
	Text txt(ss.str(), font, 40);
	txt.setOutlineThickness(GC::TEXT_OUTLINE); //Put black outline on text during gameplay
	FloatRect fr = txt.getGlobalBounds();
	txt.setPosition(window.getSize().x * 0.f + 10, window.getSize().y * 0.f + 10);
	window.draw(txt);
	
	//Draw score
	ss.str("");
	ss << "Score: " << setw(6) << setfill('0') << metrics.score; //display up to 6 digits
	txt.setString(ss.str());
	fr = txt.getGlobalBounds();
	x = window.getSize().x - fr.width - 10;
	txt.setPosition(x, window.getSize().y * 0.f + 10);
	window.draw(txt);
}

/*
renders name enter screen text
window: window that the game is displayed in
pre-con: one game cycle has passed, mode = ENTER_NAME
post-con: text has been rendered to the end screen, updates with user input
*/
void Game::RenderEnterNameHUD(RenderWindow& window)
{
	//Draw main text
	Text txt("Game over - Enter initials: " + metrics.name, font, 40);
	FloatRect fr = txt.getGlobalBounds();
	txt.setPosition(window.getSize().x / 2.f - fr.width / 2.f, window.getSize().y / 2.f - fr.height / 2.f);
	window.draw(txt);
}

/*
renders pause screen text
window: window that the game is displayed in
pre-con: one game cycle has passed, mode = PAUSE
post-con: pause screen text has been rendered
*/
void Game::RenderPauseHUD(RenderWindow& window)
{
	RenderPlayHUD(window);

	//Draw pause text
	Text txt("PAUSED", font, 100);
	txt.setOutlineThickness(GC::TEXT_OUTLINE); //Put black outline on text during gameplay
	FloatRect fr = txt.getGlobalBounds();
	txt.setPosition(window.getSize().x / 2.f - fr.width / 2.f, window.getSize().y / 2.f - fr.height / 2.f);
	window.draw(txt);
}

/*
renders end screen text
window: window that the game is displayed in
pre-con: one game cycle has passed, mode = END
post-con: text has been rendered to the end screen
*/
void Game::RenderEndHUD(RenderWindow& window)
{
	float x;
	float y;

	//Draw 'Game over' text
	Text endTxt("Game over", font, 100);
	FloatRect Efr = endTxt.getGlobalBounds();
	endTxt.setPosition(window.getSize().x / 2.f - Efr.width / 2.f, 0);
	window.draw(endTxt);

	//Draw highscore titles
	y = window.getSize().y * 0.15f;

	Text txt("Place", font, 50);
	FloatRect fr = txt.getGlobalBounds();
	x = window.getSize().x * 0.20f - fr.width / 2.f;
	txt.setPosition(x, y);
	window.draw(txt);

	txt.setString("Name");
	fr = txt.getGlobalBounds();
	x = window.getSize().x * 0.40f - fr.width / 2.f;
	txt.setPosition(x, y);
	window.draw(txt);

	txt.setString("Score");
	fr = txt.getGlobalBounds();
	x = window.getSize().x * 0.70f - fr.width / 2.f;
	txt.setPosition(x, y);
	window.draw(txt);

	y += window.getSize().y * 0.10f;

	//Draw player highscores
	for (size_t i = 0; i < 10; ++i)
	{
		stringstream ss;
		string name = "---";
		int score = 0;
		if (metrics.playerData.size() > i) 
		{
			name = metrics.playerData[i].name;
			score = metrics.playerData[i].score;
		}

		ss << i + 1;
		txt.setString(ss.str()); 
		fr = txt.getGlobalBounds();
		x = window.getSize().x * 0.20f - fr.width / 2.f;
		txt.setPosition(x, y);
		window.draw(txt);

		txt.setString(name);
		fr = txt.getGlobalBounds();
		x = window.getSize().x * 0.40f - fr.width / 2.f;
		txt.setPosition(x, y);
		window.draw(txt);

		ss.str("");
		if (name == "---")
			ss << "------";
		else
			ss << setw(6) << setfill('0') << score; //display up to 6 digits
		txt.setString(ss.str());
		fr = txt.getGlobalBounds();
		x = window.getSize().x * 0.70f - fr.width / 2.f;
		txt.setPosition(x, y);
		window.draw(txt);

		y += window.getSize().y * 0.06f;
	}

	//Draw restart instruction
	txt.setString("Press SPACE to restart");
	fr = txt.getGlobalBounds();
	y = window.getSize().y * 0.95f - fr.height;
	txt.setPosition(window.getSize().x / 2.f - fr.width / 2.f, y);
	window.draw(txt);
}

