#pragma once

#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"
#include "Utils.h"
#include "MyDB.h"

using namespace sf;

//dimensions in 2D that are whole numbers
struct Dim2Di
{
	int x, y;
};

//dimensions in 2D that are floating point numbers
struct Dim2Df
{
	float x, y;
};


/*
A box to put Games Constants in.
These are special numbers with important meanings (screen width,
ascii code for the escape key, number of lives a player starts with,
the name of the title screen music track, etc.
*/
namespace GC
{
	//game play related constants to tweak
	const Dim2Di SCREEN_RES{ 1200,800 };
	const char SPACE_BAR{ 32 };
	const char ESCAPE_KEY{ 27 };
	const double PI = 3.1415927;				//Value of pi
	const double DEGREES = 180.f;				//Degree equivalent of pi for sin, cos, tan
	const float I_ZERO = 0;						//Integer 0
	const float F_ZERO = 0.f;					//Float 0
	const char INVALID_KEY = -1;				//Invalid enter character
	const int MIN_SIZE = 1;						//Minimum vector size
	const float TEXT_OUTLINE = 2.f;				//Text outline size
	const float SCROLL_SPEED = 0.1f;			//Background scrolling speed
	const float RIGHT_ANGLE = 90.f;				//Used in sprite rotation
	const int NAME_SIZE = 3;					//Max name size

	//Gameplay constants
	const int NUM_LIVES = 3;					//Number of lives
	const float ROTATE_SPEED = (360.f / 2.5f);	//Speed at which player/bullet rotate
	const float BULLET_SPEED = 400.f;			//Spped at which bullet moves
	const float BULLET_SPAWN_INTERVAL = 0.5f;	//Rate of bullet fire
	const float ENEMY_SPAWN_INTERVAL = 1.f;		//Rate of enemy spawn
	const int TRIANGLE_SCORE = 100;				//Value of triangle enemy
	const int SQUARE_SCORE = 250;				//Value of square enemy
	const int CIRCLE_SCORE = 150;				//Value of circle enemy
	const int PENTAGON_SCORE = 300;				//Value of pentagon enemy
	const int MAX_SCORE = 999999;				//Maximum score player can achieve
}


//Taken from T014-explode
struct Metrics {
	const std::string VERSION = "1.4";
	int score;									//Player score
	int lives;									//Player lives
	std::string name;							//Player name
	bool useDB = true;							//Load from DB or txt file
	MyDB db;

	struct PlayerData {
		std::string name;						//Leaderboard name
		int score;								//Leaderboard score
	};
	std::vector<PlayerData> playerData;			//All players on leaderboard
	std::string filePath;

	void Restart();
	bool IsScoreInTopTen();
	void SortAndUpdatePlayerData();
	bool Save(const std::string& path = "") {
		return (useDB) ? DBSave(path) : FileSave(path);
	}
	bool Load(const std::string& path, bool _useDB) {
		useDB = _useDB;
		return (useDB) ? DBLoad(path) : FileLoad(path);
	}

	bool FileSave(const std::string& path = "");
	bool FileLoad(const std::string& path);
	bool DBSave(const std::string& path = "");
	bool DBLoad(const std::string& path);
};

struct Play
{
	enum class ObjT{ PLAYER, BULLET, ENEMY };							//What types can objects be?
	ObjT type = ObjT::PLAYER;											//Object type
	enum class EnemyT { TRIANGLE, SQUARE, CIRCLE, OTHER };				//What type can enemies be?
	EnemyT shape = EnemyT::OTHER;										//Default enemy type

	Sprite spr; 

	bool active = false;												//Is object active in-game?
	bool colliding = false;												//Is object colliding with another object?
	float radius = 30.f;												//Object radius
	int health = 1;														//Current object health
	int maxHealth = 1;													//Maximum object health
	float speed = 50.f;													//Movespeed of object
	int value = 0;														//How much score an object gives on destruction

	void Hit(Play&, int&, int&, Sound&, Sound&);
	void TakeDamage(int);
	
	void Init(RenderWindow&, Texture&, ObjT, EnemyT);
	void Update(RenderWindow&, float, std::vector<Play>, float&, float&, Sound&);
	void Render(RenderWindow&, bool);

	void InitPlayer(RenderWindow&);
	void UpdatePlayer(float);
		
	void InitBullet(RenderWindow&);
	void UpdateBullet(RenderWindow&, float, std::vector<Play>, float&, Sound&);
		
	void InitEnemy(RenderWindow&, EnemyT);
	void UpdateEnemy(RenderWindow&, float, std::vector<Play>, float&);
	void SpawnEnemy(RenderWindow&);
};

struct Game
{
	//General
	enum class Mode { START, PLAY, PAUSE, ENTER_NAME, END };	//What screens can the game present?
	Mode screen = Mode::START;									//First screen displayed

	Metrics metrics;
	Clock clock;												//Keep track of time
	float elapsed = 0.f;										//How much time has passed?
	Font font;													//Font used when displaying text

	bool debug = false;											//Run in debug mode

	void Init(RenderWindow&);
	void Update(RenderWindow&, char);
	void Render(RenderWindow&);
	
	//Start
	Sprite startTriangle, startSquare, startCircle;				//Sprites for use on start screen

	void UpdateStart(char);
	void RenderStartHUD(RenderWindow&);

	//Play
	std::vector<Play> objects;									//Store objects used during gameplay

	Texture bgndTex;											//Background texture
	Sprite bgndSpr;												//Background sprite
	FloatRect bgndRect;											//Background rect

	Texture charTex, bulletTex;									//Player-controlled textures
	Texture eSquareTex, ePentagonTex, eTriangleTex, eCircleTex;	//Enemy textures

	Music menuMusic;												//Background music	
	Music bgMusic;												//Background music	

	SoundBuffer playerBuffer;									//Player sound
	Sound playerHurt;											//

	SoundBuffer bulletBuffer;									//Bullet sound
	Sound bulletShoot;											//
	
	SoundBuffer enemyBuffer;									//Enemy sound
	Sound enemyHurt;											//

	float bulletSpawnTimer = 0.f;								//Time since last bullet spawn
	float enemySpawnTimer = 0.f;								//Time since last enemy spawn

	void CheckCollisions(RenderWindow&);

	void UpdatePlay(RenderWindow&, char);
	void RenderPlayHUD(RenderWindow&);

	//Pause
	void RenderPauseHUD(RenderWindow&);

	//Enter name
	float transition = 0.f;										//wait before providing input

	void UpdateEnterName(char);
	void RenderEnterNameHUD(RenderWindow&);

	//End
	void RenderEndHUD(RenderWindow&);
};

bool LoadTexture(const std::string& file, Texture&);
void DrawCircle(RenderWindow&, const Vector2f&, float, Color);
bool CircleToCircle(const Vector2f&, const Vector2f&, float);