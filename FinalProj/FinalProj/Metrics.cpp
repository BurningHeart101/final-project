#include "SFML/Graphics.hpp"
#include "Game.h"

#include <assert.h>
#include <string>
#include <math.h>
#include <sstream>
#include <iomanip>
#include <fstream>

using namespace sf;
using namespace std;

void Metrics::SortAndUpdatePlayerData() {
	PlayerData d{ name, score };
	bool inserted = false;

	if (playerData.size() < 10 && !inserted) //Add player if less than 10 registered
	{
		playerData.push_back(d);
		inserted = true;
	}

	if (playerData.back().score < d.score && !inserted) //Add player if within top 10
	{
		playerData.back() = d;
		inserted = true;
	}

	//Bubble sort
	for (size_t i = playerData.size() - 1; i > 0; --i)
		if (playerData[i].score > playerData[i - 1].score)
		{
			PlayerData temp = playerData[i];
			playerData[i] = playerData[i - 1];
			playerData[i - 1] = temp;
		}
}

//Code copied from T014-explode
bool Metrics::DBLoad(const std::string& path)
{
	playerData.clear();
	bool exists;
	db.Init(path, exists);
	if (exists)
	{
		db.ExecQuery("SELECT * FROM GAME_INFO");
		string version = db.GetStr(0, "VERSION");
		if (version != Metrics::VERSION)
		{
			db.ExecQuery("DROP TABLE IF EXISTS HIGHSCORES");
			db.ExecQuery("DROP TABLE IF EXISTS GAME_INFO");
			exists = false;
		}
	}

	if (!exists)
	{
		db.ExecQuery("CREATE TABLE HIGHSCORES(" \
			"ID		INTEGER PRIMARY KEY		,"\
			"NAME			TEXT	NOT NULL,"\
			"SCORE			INT		NOT NULL)");


		db.ExecQuery("CREATE TABLE GAME_INFO("\
			"ID		INTEGER PRIMARY KEY		,"\
			"VERSION		CHAR(10)	NOT NULL)");


		stringstream ss;
		ss << "INSERT INTO GAME_INFO (VERSION) "\
			"VALUES (" << Metrics::VERSION << ")";
		db.ExecQuery(ss.str());
		return false;
	}

	db.ExecQuery("SELECT * FROM HIGHSCORES");
	for (size_t i = 0; i < db.results.size(); ++i)
		playerData.push_back(PlayerData{ db.GetStr(i,"NAME"), db.GetInt(i, "SCORE") });
	return true;
}

bool Metrics::FileSave(const std::string& path) {

	if (!path.empty())
		filePath = path;
	ofstream fs;
	fs.open(filePath);
	if (fs.is_open() && fs.good())
	{
		fs << VERSION;
		for (size_t i = 0; i < playerData.size(); ++i)
		{
			fs << ' ' << playerData[i].name << ' ' << playerData[i].score;
		}
		assert(!fs.fail());
		fs.close();
	}
	else
	{
		assert(false);
		return false;
	}
	return true;
}

bool Metrics::DBSave(const std::string& path) 
{
	db.ExecQuery("DELETE FROM HIGHSCORES");
	stringstream ss;
	for (size_t i = 0; i < playerData.size(); ++i)
	{
		ss.str("");
		ss << "INSERT INTO HIGHSCORES (NAME,SCORE)" \
			<< "VALUES ('" << playerData[i].name << "'," << playerData[i].score << ")";
		db.ExecQuery(ss.str());
	}
	ss.str("");
	ss << "UPDATE GAME_INFO SET VERSION = " << Metrics::VERSION;
	db.ExecQuery(ss.str());
	db.SaveToDisk();
	return false;
}

bool Metrics::FileLoad(const std::string& path) 
{

	assert(!path.empty());
	filePath = path;
	ifstream fs;
	fs.open(filePath, ios::binary);
	if (fs.is_open() && fs.good())
	{
		string version;
		fs >> version;
		if (version == VERSION)
		{
			playerData.clear();
			while (!fs.eof()) {
				PlayerData d;
				fs >> d.name;
				assert(!d.name.empty());
				fs >> d.score;
				assert(d.score >= 0);
				playerData.push_back(d);
			}
		}
		assert(!fs.fail());
		fs.close();
	}
	return false;
}

bool Metrics::IsScoreInTopTen() 
{
	if (playerData.size() < 10)
		return true;
	return playerData.back().score < score;
}

void Metrics::Restart() 
{
	score = 0;
	lives = GC::NUM_LIVES;
	name = "";
}