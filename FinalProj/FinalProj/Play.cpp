#include "SFML/Graphics.hpp"
#include "Game.h"

#include <assert.h>
#include <string>
#include <math.h>

using namespace sf;

/*
initialises an object used for when the player is playing the game
window: window that the game is displayed in
tex: texture being applied to the object
type_: assigned object type used to initialise
shape: assigned enemy type used to initialise enemy
pre-con: object needs to be initialised
post-con: object is initialised with correct properties
*/
void Play::Init(RenderWindow& window, Texture& tex, ObjT type_, EnemyT shape_)
{
	//Set types
	type = type_;
	shape = shape_;

	//Set textures
	spr.setTexture(tex);
	spr.setOrigin(tex.getSize().x / 2.f, tex.getSize().y / 2.f);

	switch (type_)
	{
	case ObjT::PLAYER:
		type = ObjT::PLAYER;
		InitPlayer(window);
		break;

	case ObjT::BULLET:
		type = ObjT::BULLET;
		InitBullet(window);
		break;

	case ObjT::ENEMY:
		type = ObjT::ENEMY;
		InitEnemy(window, shape_);
		break;

	default:
		assert(false);
	}

	health = maxHealth;
}

/*
initialises player object
window: window that the game is displayed in
pre-con: object with PLAYER type has been passed through
post-con: player is initalised with correct properties
*/
void Play::InitPlayer(RenderWindow& window)
{
	radius = 30.f;

	spr.setPosition(window.getSize().x / 2.f, window.getSize().y / 2.f); //Set player in middle of screen
	active = true; //Player is active when game starts
}

/*
initialises bullet object
window: window that the game is displayed in
pre-con: object with BULLET type has been passed through
post-con: bullet is initalised with correct properties
*/
void Play::InitBullet(RenderWindow& window)
{
	radius = 10.f;
}

/*
initialises enemy object
window: window that the game is displayed in
type: type of enemy
pre-con: object with ENEMY type has been passed through
post-con: enemy is initalised with correct properties depending on their shape
*/
void Play::InitEnemy(RenderWindow& window, EnemyT shape)
{
	radius = 25.f;

	//Edit enemy based on type
	switch (shape)
	{
	case EnemyT::TRIANGLE:
		//Generic enemy
		value = GC::TRIANGLE_SCORE;
		break;

	case EnemyT::SQUARE:
		//Slower but takes more hits
		speed *= 0.6f; 
		maxHealth = health * 3;
		value = GC::SQUARE_SCORE;
		break;

	case EnemyT::CIRCLE:
		//Smaller, harder to hit
		radius = 15.f;
		value = GC::CIRCLE_SCORE;
		break;

	default:
		assert(false);
	}
}

/*
updates object
window: window that the game is displayed in
elapsed: time since previous game cycle
objects: vector that stores all game objects inside
bulletSpawnTimer: time since last bullet spawn
enemySpawnTimer: time since last enemy spawn
pre-con: game is currently on PLAY screen
post-con: object has been updated with correct properties
*/
void Play::Update(RenderWindow& window, float elapsed, std::vector<Play> objects, float& bulletSpawnTimer, float& enemySpawnTimer, Sound& bulletShoot)
{
	colliding = false; //Reset any previously set collision flags

	//Update objects based on type
	switch (type)
	{
	case ObjT::PLAYER:
		UpdatePlayer(elapsed);
		break;

	case ObjT::BULLET:
		UpdateBullet(window, elapsed, objects, bulletSpawnTimer, bulletShoot);
		break;

	case ObjT::ENEMY:
		UpdateEnemy(window, elapsed, objects, enemySpawnTimer);
		break;

	default:
		assert(false);
	}
}

/*
updates player object
elapsed: time since previous game cycle
pre-con: player object selected to be updated
post-con: player object has updated based on user input
*/
void Play::UpdatePlayer(float elapsed)
{
	float rot = spr.getRotation();

	//Rotate clockwise
	if (Keyboard::isKeyPressed(Keyboard::D))
		rot += GC::ROTATE_SPEED * elapsed;

	//Rotate anti-clockwise
	if (Keyboard::isKeyPressed(Keyboard::A))
		rot -= GC::ROTATE_SPEED * elapsed;

	spr.setRotation(rot);
}

/*
updates bullet object
window: window that the game is displayed in
elapsed: time since previous game cycle
objects: vector that stores all game objects inside
bulletSpawnTimer: time since last bullet spawn
pre-con: bullet object selected to be updated
post-con: bullet object has updated based on user input
*/
void Play::UpdateBullet(RenderWindow& window, float elapsed, std::vector<Play> objects, float& bulletSpawnTimer, Sound& bulletShoot)
{
	Play ship = objects[0]; //Get player from objects vector for referencing

	//If firing, set bullet to be active
	if ((Keyboard::isKeyPressed(Keyboard::K) && (bulletSpawnTimer >= GC::BULLET_SPAWN_INTERVAL) && (!active)))
	{
		//Spawn on ship, face same way as ship
		spr.setPosition(ship.spr.getPosition());
		spr.setRotation(ship.spr.getRotation());
		bulletShoot.play();

		active = true;
		bulletSpawnTimer = GC::F_ZERO;
	}

	//If bullet is active, update bullet
	if (active == true)
	{
		Vector2f pos = spr.getPosition();
		double a = spr.getRotation() * (GC::PI/GC::DEGREES); //Convert from degrees to radians for sin, cos, tan

		//Move bullet
		pos.x += sin(a) * GC::BULLET_SPEED * elapsed;
		pos.y += -cos(a) * GC::BULLET_SPEED * elapsed;
		spr.setPosition(pos);

		//Check if bullet has moved offscreen, set inactive if so
		Vector2u screenSz = window.getSize();
		if ((pos.y < screenSz.y * 0.f) || (pos.y > screenSz.y) || (pos.x < screenSz.x * 0.f) || (pos.x > screenSz.x))
			active = false;
	}
}

/*
updates enemy object
window: window that the game is displayed in
elapsed: time since previous game cycle
objects: vector that stores all game objects inside
enemySpawnTimer: time since last enemy spawn
pre-con: enemy object selected to be updated
post-con: enemy object has updated
*/
void Play::UpdateEnemy(RenderWindow& window, float elapsed, std::vector<Play> objects, float& enemySpawnTimer)
{
	Play ship = objects[0]; //Get player from objects vector for referencing

	//If not active and timer has passed, spawn enemy
	if ((!active) && (enemySpawnTimer > GC::ENEMY_SPAWN_INTERVAL))
	{
		SpawnEnemy(window);
		enemySpawnTimer = GC::F_ZERO;
	}

	//Get positions for enemy and ship
	Vector2f pos = spr.getPosition();
	Vector2f shipPos = ship.spr.getPosition();

	//Get angle towards player
	//TOA: tanx = Opposite / Adjacent
	float A = shipPos.x - pos.x; //Adjacent
	float O = shipPos.y - pos.y; //Opposite
	float angle = (atan2(O, A)); //x = Tan^-1 (Opposite / Adjacent)
	angle *= GC::DEGREES / GC::PI; //Convert from radians to degrees

	//Move enemy towards player
	pos.x += sin(angle) * speed * elapsed;
	pos.y += -cos(angle) * speed * elapsed;

	spr.setPosition(pos);
	angle += GC::RIGHT_ANGLE; //Update angle for use in sprite rotation
	spr.setRotation(angle);
}

/*
spawn an enemy on a random side of the screen, and set them to be active again
window: window that the game is displayed in
pre-con: enemy needs to be spawned in-game
post-con: enemy has been spawned on a random side of the screen, properties are reset
*/
void Play::SpawnEnemy(RenderWindow& window)
{
	//Choose a random side to spawn on
	int screenSide = rand() % 4;
	int x, y;

	switch (screenSide)
	{
	case 0: //Top side
		x = rand() % GC::SCREEN_RES.x;
		y = (window.getSize().y * 0.f) - (radius);
		break;

	case 1: //Bottom side
		x = rand() % GC::SCREEN_RES.x;
		y = (window.getSize().y) + (radius);
		break;

	case 2: //Left side
		x = (window.getSize().x * 0.f) - (radius);
		y = rand() % GC::SCREEN_RES.y;
		break;

	case 3: //Right side
		x = (window.getSize().x) + (radius);
		y = rand() % GC::SCREEN_RES.y;
		break;

	default:
		assert(false);
	}

	//Reset enemy health
	health = maxHealth;

	spr.setPosition(x, y);
	active = true;
}

/*
apply damage to a game object
amount: damage that the object should take from a hit
pre-con: object has collided with another object, enemies & enemies excluded
post-con: objects health is reduced by amount
*/
void Play::TakeDamage(int amount)
{
	health -= amount;
	if (health <= GC::I_ZERO)
		active = false;
}

/*
make a game object take damage
other: game object currently colliding with
lives: current lives player has
score: current score player has
pre-con: object has collided with another object
post-con: objects has taken damage, lives & score has been updated
*/
void Play::Hit(Play& other, int& lives, int& score, Sound& playerHurt, Sound& enemyHurt)
{
	switch (type)
	{
	case ObjT::PLAYER:
		if (other.type == ObjT::ENEMY)
		{
			--lives;
			playerHurt.play();
			other.TakeDamage(999); //Destroy enemy
		}
		break;

	case ObjT::BULLET:
		if (other.type == ObjT::ENEMY)
		{
			TakeDamage(999); //Destroy bullet
			other.TakeDamage(1);
			if (other.health <= GC::I_ZERO)
				score += other.value;
			enemyHurt.play();
		}
		break;

	case ObjT::ENEMY:
			break;

	default:
		assert(false);
	}
	
	//Make sure player doesn't exceed max score
	if (score > GC::MAX_SCORE)
		score = GC::MAX_SCORE;
}

/*
render game object
window: window that the game is displayed in
pre-con: object has been updated
post-con: object has been rendered with updated properties
*/
void Play::Render(RenderWindow& window, bool debug)
{
	if (active)
	{
		if (debug) //If in debug mode and object active, draw circle around object
		{
			Color col = Color::Green;
			if (colliding)
				col = Color::Red;
			DrawCircle(window, spr.getPosition(), radius, col);
		}
		window.draw(spr);
	}
}

/*
draw debug circles around game objects
window: window that the game is displayed in
pos: location of game objects
radius: radius of game object
col: colour to set circle
pre-con: debug mode is enabled
post-con: debug circle properly drawn around game object
*/
void DrawCircle(RenderWindow& window, const Vector2f& pos, float radius, Color col)
{
	CircleShape c;
	c.setRadius(radius);
	c.setPointCount(20);
	c.setOutlineColor(col);
	c.setOutlineThickness(2);
	c.setFillColor(Color::Transparent);
	c.setPosition(pos);
	c.setOrigin(radius, radius);
	window.draw(c);
}

/*
check distance between objects using pythagorus
pos1: object 1's position
pos2: object 2's position
minDist: minimum possible distance between object 1 and object 2 (radius+radius)
pre-con: object collision is being checked
post-con: returns whether 2 objects are colliding or not
*/
bool CircleToCircle(const Vector2f& pos1, const Vector2f& pos2, float minDist)
{
	//Pythagorus: a^2 + b^2 = c^2
	float dist = (pos1.x - pos2.x) * (pos1.x - pos2.x) + (pos1.y - pos2.y) * (pos1.y - pos2.y);
	dist = sqrtf(dist);

	return dist <= minDist; //If less than minDist between objects, they must be intersecting
}

/*
load texture
file: location of texture to be loaded
tex: texture to load into
pre-con: game object is trying to load texture
post-con: texture is properly loaded
*/
bool LoadTexture(const std::string& file, Texture& tex)
{
	if (tex.loadFromFile(file))
	{
		tex.setSmooth(true);
		return true;
	}
	assert(false);
	return false;
}
