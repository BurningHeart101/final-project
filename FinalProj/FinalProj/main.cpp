#include "SFML/Graphics.hpp"
#include "Game.h"
using namespace sf;

int main()
{
	srand((unsigned int)time(0));

	// Create the main window
	RenderWindow window(VideoMode(GC::SCREEN_RES.x, GC::SCREEN_RES.y), "Shapeshooter");
	Game game;

	game.Init(window);

	// Start the game loop 
	while (window.isOpen())
	{
		char key = -1;
		// Process events
		Event event;
		while (window.pollEvent(event))
		{
			// Close window: exit
			if (event.type == Event::Closed) 
				window.close();
				
			if (event.type == Event::TextEntered)
			{
				if (event.text.unicode == GC::ESCAPE_KEY)
					window.close();

				//Copied from T014
				if (isdigit(event.text.unicode) || isalpha(event.text.unicode))
				{
					key = static_cast<char>(event.text.unicode);
					key = toupper(key);
				}

				if (event.text.unicode == GC::SPACE_BAR)
					key = GC::SPACE_BAR;
			}
		} 
		// Clear screen
		window.clear();

		//Update game objects
		game.Update(window, key);
		game.Render(window);

		// Update the window
		window.display();
	}

	return EXIT_SUCCESS;
}
